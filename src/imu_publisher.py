#!/usr/bin/env python
import sys, numpy as np
import time
import rospy, rosbag
from std_msgs.msg import String, UInt8, Header, MultiArrayLayout, MultiArrayDimension, Float64MultiArray
from bento_trigno.msg import *
from bento_trigno.srv import trigno
from rospy.numpy_msg import numpy_msg
from bento_controller.msg import *
from bento_controller.srv import *
"""
Tests communication with and data acquisition from a Delsys Trigno wireless
EMG system.

Delsys Trigno Control Utility needs to be installed and running, and the device
needs to be plugged in.

The tests run by this script are very simple and are by no means exhaustive. It
just sets different numbers of channels and ensures the data received is the
correct shape.
"""

def rossservice_check(service_name='/trigno_data_streaming', modality = 'IMU'):
    # ROS Service to check if Trigno has started to stream data
    rospy.wait_for_service(service_name)
    trigno_check = rospy.ServiceProxy(service_name, trigno)
    resp = trigno_check()
    while not resp.trigno_check:
        resp = trigno_check()
    print "Trigno is streaming " + modality + " data", resp.trigno_check

try:
    import daq
except ImportError:
    sys.path.insert(0, '..')
    from pygesture import daq

trigno_connected = False
start = time.time()
stop = time.time()

# ROS initialization, publisher & service definition
rospy.init_node('Trigno_signals', anonymous=True)

# Initialize parameters
ros_prefix = "delsys_trigno" # Identifier for ROS parameters

if rospy.has_param(ros_prefix):  # Check if ROS params exist
    config = rospy.get_param(ros_prefix)
    exptTime = config["expt_time"]
    window_length = config["num_imu_samples"]
    host_ip = config["host_ip"]
    pub_name = rospy.get_param("~pub_name")
    modality = rospy.get_param("~modality")
    if modality == "imu":
        num_channels = config["imu_channels"]
    elif modality == "acc":
        num_channels = config["acc_channels"]
    else:
        num_channels = config["quat_channels"]
    rossservice_check('/EMG/trigno_data_streaming', modality)
else:
    print "ROS Params not found"
    exptTime = 300
    num_channels = 144
    window_length = 1
    host_ip = '10.0.1.120'
    pub_name = '/trigno/imu'
    modality = 'imu'
    rossservice_check()

imuPub = rospy.Publisher(pub_name, imuArray, queue_size=1)
# test multi-channel
dev = daq.Trigno_IMU((0, num_channels-1), window_length, host_ip, modality)
while stop - start < exptTime :
    data = dev.read()
    assert data.shape == (num_channels, window_length)

    stop = time.time()

    # Publish Data as ROS Topic
    h = Header()
    h.stamp = rospy.Time.now()
    h.frame_id = "Delsys Trigno " + modality
    data = data.flatten()

    #print stop-start, len(data)
    imuPub.publish(imuArray(h,data.tolist()))
dev.stop()

