#!/usr/bin/env python

# Working code for pronation and suppination of the wrist
import argparse
import rospy
from std_msgs.msg import *
from bento_controller.srv import *
from bento_controller.msg import *
from bento_trigno.msg import *
from bento_trigno.srv import *
import pickle
from sensor_msgs.msg import Joy
import numpy as np
# from twitchy.msg import *
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
from sensor_msgs.msg import Imu
from math import *
import time
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
from geometry_msgs.msg import Vector3

tic = time.time()
twitchy_state = np.zeros(5)
geometry_pub = rospy.Publisher('/trigno_wrist', Vector3, queue_size=1)


def euler_pronation(q):
    # Rotation order 3-2-1
    y = -2 * q[1] * q[2] + 2 * q[0] * q[3]
    x = q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]
    phi = atan2(y, x)  # Yaw
    theta = asin(2 * q[1] * q[3] + 2 * q[0] * q[2])  # Pitch
    psi = atan2(-2 * q[2] * q[3] + 2 * q[0] * q[1], q[3] * q[3] - q[2] * q[2] - q[1] * q[1] + q[0] * q[0])  # Roll

    # if x < 0 and y >= 0:
    # 	phi -= 1.5*pi


    return (phi, theta, psi)


def get_quaternion_pronation(phi, theta, psi):
    q = np.zeros(4)
    phi /= 2
    theta /= 2
    psi /= 2

    q[0] = cos(phi) * cos(theta) * cos(psi) - sin(phi) * sin(theta) * sin(psi)
    q[1] = cos(phi) * cos(theta) * sin(psi) + sin(phi) * sin(theta) * cos(psi)
    q[2] = cos(phi) * sin(theta) * cos(psi) - sin(phi) * cos(theta) * sin(psi)
    q[3] = cos(phi) * sin(theta) * sin(psi) + sin(phi) * cos(theta) * cos(psi)
    return q


def euler_adduction(q):
    # Rotation order 2-3-1
    phi = atan2(2 * q[2] * q[3] + 2 * q[0] * q[1], q[3] * q[3] - q[2] * q[2] - q[1] * q[1] + q[0] * q[0])  # Roll
    theta = -asin(2 * q[1] * q[3] - 2 * q[0] * q[2])  # Pitch
    psi = atan2(2 * q[1] * q[2] + 2 * q[0] * q[3], q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3])  # Yaw
    return (phi, theta, psi)


def get_quaternion_adduction(phi, theta, psi):
    q = np.zeros(4)
    phi /= 2
    theta /= 2
    psi /= 2

    q[0] = cos(phi) * cos(theta) * cos(psi) + sin(phi) * sin(theta) * sin(psi)
    q[1] = -cos(phi) * sin(theta) * sin(psi) + sin(phi) * cos(theta) * cos(psi)
    q[2] = cos(phi) * sin(theta) * cos(psi) + sin(phi) * cos(theta) * sin(psi)
    q[3] = cos(phi) * cos(theta) * sin(psi) - sin(phi) * sin(theta) * cos(psi)
    return q


def euler_flexion(q):
    # Rotation order 1-2-3
    phi = atan2(2 * q[1] * q[3] + 2 * q[0] * q[2], q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3])  # Pitch
    theta = -asin(2 * q[1] * q[2] - 2 * q[0] * q[3])  # Yaw
    psi = atan2(2 * q[2] * q[3] + 2 * q[0] * q[1], - q[3] * q[3] + q[2] * q[2] - q[1] * q[1] + q[0] * q[0])  # Roll
    return (phi, theta, psi)


def get_quaternion_flexion(phi, theta, psi):
    q = np.zeros(4)
    phi /= 2
    theta /= 2
    psi /= 2

    q[0] = cos(phi) * cos(theta) * cos(psi) + sin(phi) * sin(theta) * sin(psi)
    q[1] = cos(phi) * cos(theta) * sin(psi) - sin(phi) * sin(theta) * cos(psi)
    q[2] = -cos(phi) * sin(theta) * sin(psi) + sin(phi) * cos(theta) * cos(psi)
    q[3] = cos(phi) * sin(theta) * cos(psi) + sin(phi) * cos(theta) * sin(psi)
    return q


def quaternion_matrix(q):
    matrix = np.zeros((3, 3))
    matrix[0][0] = q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]
    matrix[0][1] = 2 * q[1] * q[2] + 2 * q[0] * q[3]
    matrix[0][2] = 2 * q[1] * q[3] - 2 * q[0] * q[2]
    matrix[1][0] = 2 * q[1] * q[2] - 2 * q[0] * q[3]
    matrix[1][1] = q[0] * q[0] - q[1] * q[1] + q[2] * q[2] - q[3] * q[3]
    matrix[1][2] = 2 * q[2] * q[3] + 2 * q[0] * q[1]
    matrix[2][0] = 2 * q[1] * q[3] + 2 * q[0] * q[2]
    matrix[2][1] = 2 * q[2] * q[3] - 2 * q[0] * q[1]
    matrix[2][2] = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]
    return matrix


def inverse_kinematics_quaternion(R):
    if R[1][1] > -R[2][2] and R[0][0] > -R[1][1] and R[0][0] > -R[2][2]:
        q = np.zeros(4)
        dr = sqrt(1 + R[0][0] + R[1][1] + R[2][2])
        q[0] = dr
        q[1] = (R[1][2] - R[2][1]) / dr
        q[2] = (R[2][0] - R[0][2]) / dr
        q[3] = (R[0][1] - R[1][0]) / dr
        q *= 0.5
        return q

    elif R[1][1] < -R[2][2] and R[0][0] > R[1][1] and R[0][0] > R[2][2]:
        q = np.zeros(4)
        dr = sqrt(1 + R[0][0] - R[1][1] - R[2][2])
        q[0] = (R[1][2] - R[2][1]) / dr
        q[1] = dr
        q[2] = (R[0][1] + R[1][0]) / dr
        q[3] = (R[2][0] + R[0][2]) / dr
        q *= 0.5
        return q

    elif R[1][1] > R[2][2] and R[0][0] < R[1][1] and R[0][0] < -R[2][2]:
        q = np.zeros(4)
        dr = sqrt(1 - R[0][0] + R[1][1] - R[2][2])
        q[0] = (R[2][0] - R[0][2]) / dr
        q[1] = (R[0][1] + R[1][0]) / dr
        q[2] = dr
        q[3] = (R[1][2] + R[2][1]) / dr
        q *= 0.5
        return q

    elif R[1][1] < R[2][2] and R[0][0] < -R[1][1] and R[0][0] < R[2][2]:
        q = np.zeros(4)
        dr = sqrt(1 - R[0][0] - R[1][1] + R[2][2])
        q[0] = (R[0][1] - R[1][0]) / dr
        q[1] = (R[2][0] + R[0][2]) / dr
        q[2] = dr
        q[3] = (R[1][2] + R[2][1]) / dr
        q *= 0.5
        return q
    else:
        print "Error in the given matrix !!"


def rotate_by_quaternion(q, R):  # Index represents the x, y or z axis
    Rx = quaternion_matrix(q)
    return np.dot(Rx, R)


def pronation(q_ab, R_ab, reference_angles):
    iPhi, iTheta, iPsi = euler_pronation(q_ab)
    # print degrees(iPhi), degrees(reference_angles[0]), degrees(iTheta), \
    # degrees(reference_angles[1]), degrees(iPsi), degrees(reference_angles[2])
    invert_q = get_quaternion_pronation(0, 0, -iPsi)
    R32 = rotate_by_quaternion(invert_q, R_ab)
    invert_q = get_quaternion_pronation(0, -iTheta, 0)
    R3 = rotate_by_quaternion(invert_q, R32)
    q_ypr = inverse_kinematics_quaternion(R3)
    yaw, pitch, roll = euler_pronation(q_ypr)
    '''
    yaw = degrees(yaw)
    pitch = degrees(pitch)
    roll = degrees(roll)
    print pitch, yaw, roll
    '''

    if iPsi > -pi and iPsi < -2.8:
        iPsi = -iPsi

    geometry_pub.publish(Vector3(iPhi, iTheta, iPsi))
    return iPhi  # yaw


def flexion(q_ab, R_ab, reference_angles):
    iPhi, iTheta, iPsi = euler_flexion(q_ab)
    # print degrees(iPhi), degrees(reference_angles[0]), degrees(iTheta), \
    # degrees(reference_angles[1]), degrees(iPsi), degrees(reference_angles[2])
    invert_q = get_quaternion_flexion(0, 0, -iPsi)
    R32 = rotate_by_quaternion(invert_q, R_ab)
    invert_q = get_quaternion_flexion(0, -iTheta, 0)
    R3 = rotate_by_quaternion(invert_q, R32)
    q_ypr = inverse_kinematics_quaternion(R3)
    pitch, yaw, roll = euler_flexion(q_ypr)
    '''
    yaw = degrees(yaw)
    pitch = degrees(pitch)
    roll = degrees(roll)
    print pitch, yaw, roll
    '''
    return pitch


def adduction(q_ab, R_ab, reference_angles):
    iPhi, iTheta, iPsi = euler_adduction(q_ab)
    # rint degrees(iPhi), degrees(reference_angles[0]), degrees(iTheta), \
    # degrees(reference_angles[1]), degrees(iPsi), degrees(reference_angles[2])
    invert_q = get_quaternion_adduction(0, 0, -iPsi)
    R32 = rotate_by_quaternion(invert_q, R_ab)
    invert_q = get_quaternion_adduction(0, -iTheta, 0)
    R3 = rotate_by_quaternion(invert_q, R32)
    q_ypr = inverse_kinematics_quaternion(R3)
    roll, pitch, yaw = euler_adduction(q_ypr)
    '''
    yaw = degrees(yaw)
    pitch = degrees(pitch)
    roll = degrees(roll)
    # print roll, pitch, yaw
    '''
    return roll

def state_callback(msg):
    global data
    quat = np.asarray(msg.imu, dtype='f')
    quat = np.transpose(quat.reshape((80, quat.shape[0]/80)))
    quat = np.mean(quat, axis=0)
    data = quat
    #print quat, quat.shape

if __name__ == "__main__":

    rospy.init_node("listener")
    global mat, data
    # pause_srv = rospy.ServiceProxy("/bento/pause", Pause)
    # pause_srv.call(False)
    sub = rospy.Subscriber("/trigno/quat", imuArray, state_callback)
    pub = rospy.Publisher('/trigno_pry', Floats, queue_size=10)
    rospy.sleep(3)
    wrist_angles = np.zeros(3)

    ros_prefix = 'trigno_proportional_emg'
    if rospy.has_param(ros_prefix):
        config = rospy.get_param(ros_prefix)
        trigno_id = int(config['wrist_unit'])
    else:
        trigno_id = 5
    print "-------- Trigno_ID = ", trigno_id
    quat_ind = 5*(trigno_id-1)
    flag = 0
    while not rospy.is_shutdown():
        toc = time.time()
        q = data[quat_ind:quat_ind+4]
        phi, theta, psi = euler_pronation(q)
        if toc - tic < 5:
            reference_frame = quaternion_matrix(q)
            reference_frame = reference_frame.transpose()
            reference_angles = (phi, theta, psi)
        else:
            if not flag:
                print "Calibration complete"
                flag = 1
            current_frame = quaternion_matrix(q)
            R_ab = np.dot(reference_frame, current_frame)
            q_ab = inverse_kinematics_quaternion(R_ab)

            # Converting to Euler so that it's easier to visualize
            roll = pronation(q_ab, R_ab, reference_angles)
            pitch = flexion(q_ab, R_ab, reference_angles)
            yaw = adduction(q_ab, R_ab, reference_angles)
            '''
            yaw = degrees(yaw)
            pitch = degrees(pitch)
            roll = degrees(roll)
            if yaw < 0: yaw = 360 + yaw
            if pitch < 0: pitch = 360 + pitch
            if roll < 0: roll = 360 + roll
            #print "Yaw = ", yaw, "Pitch = ", pitch, "Roll = ", roll
            '''
            wrist_angles[0] = yaw
            wrist_angles[1] = pitch
            wrist_angles[2] = roll
            pub.publish(wrist_angles)
    rospy.spin()

