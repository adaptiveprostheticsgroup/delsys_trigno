#!/usr/bin/env python
import sys, numpy as np
import time
import rospy, rosbag
from std_msgs.msg import String, UInt8, Header, MultiArrayLayout, MultiArrayDimension, Float64MultiArray
from bento_trigno.msg import *
from bento_trigno.srv import trigno
from rospy.numpy_msg import numpy_msg
from bento_controller.msg import *
from bento_controller.srv import *

"""
Tests communication with and data acquisition from a Delsys Trigno wireless
EMG system.

Delsys Trigno Control Utility needs to be installed and running, and the device
needs to be plugged in.

The tests run by this script are very simple and are by no means exhaustive. It
just sets different numbers of channels and ensures the data received is the
correct shape.
"""

try:
    import daq
except ImportError:
    sys.path.insert(0, '..')
    from pygesture import daq

trigno_connected = False
start = time.time()
stop = time.time()

# ROS initialization, publisher & service definition
rospy.init_node('Trigno_EMG_signals', anonymous=True)


def trigno_check(req):     # Check whether trigno is steaming data
    global trigno_connected
    return trigno_connected
s = rospy.Service('trigno_data_streaming', trigno, trigno_check)

# Initialize parameters
ros_prefix = "delsys_trigno" # Identifier for ROS parameters

if rospy.has_param(ros_prefix):  # Check if ROS params exist
    config = rospy.get_param(ros_prefix)
    exptTime = config["expt_time"]
    num_channels = config["emg_channels"]
    window_length = config["num_emg_samples"]
    host_ip = config["host_ip"]
    pub_name = rospy.get_param("~pub_name")
else:
    exptTime = 300
    num_channels = 16
    window_length = 300
    host_ip = '10.0.1.120'
    pub_name = '/trigno/emg'

emgPub = rospy.Publisher(pub_name, emgArray, queue_size=10)
# test multi-channel
dev = daq.TrignoDaq((0, num_channels-1), window_length, host_ip)
dev.start()
while stop - start < exptTime :
    data = dev.read()
    assert data.shape == (num_channels, window_length)
    trigno_connected = True
    stop = time.time()
    print stop-start, len(data[0])

    # Publish Data as ROS Topic
    h = Header()
    h.stamp = rospy.Time.now()
    h.frame_id = "Delsys Trigno EMG "
    data = data.flatten()
    emgPub.publish(emgArray(h,data.tolist()))
dev.stop()

